# DATAtourisme ontology
Tourism national ontology structuring in a common sharing format the whole tourism data extracted from different official French data bases : entertainment and events, natural and cultural sites, leisure and sports activities, touristic products, tours, accomodations, shops, restaurants …

DATAtourisme is a collaborative project. We encourage you to send us your comments and suggestions in order to prepare an amended version. For further information : http://www.datatourisme.fr

---------------------------

Ontologie destinée à structurer les données décrivant l’ensemble des points d’intérêts dits « touristiques » recensés par les Offices de Tourisme, Agences Départementales et Comités Régionaux du Tourisme : fêtes et manifestations, sites patrimoniaux, activités de loisirs, itinéraires, hébergements, boutiques ou encore restaurants. Ce format de référence est notamment utilisé par la plateforme nationale DATAtourisme pour la diffusion en OpenData des données touristiques institutionnelles des destinations françaises.

DATAtourisme est un projet collaboratif, destiné à s’enrichir des contributions de ses usagers. Merci de nous faire part de vos remarques et suggestions directement ici, ou via http://www.datatourisme.fr
